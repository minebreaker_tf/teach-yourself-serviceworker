const gulp = require("gulp")
const gulpTypescript = require("gulp-typescript")
const gulpBabel = require("gulp-babel")
const rollup = require("rollup")
const rollupNodeResolve = require("rollup-plugin-node-resolve")
const rollupCommonJs = require("rollup-plugin-commonjs")
const rollupReplace = require("rollup-plugin-replace")
const rimraf = require("rimraf")

const config = {
    // TypeScriptソース
    srcDir: "src/main/typescript",
    srcTestDir: "src/test/typescript",
    // ビルド出力先
    buildBase: "build/typescript",
    compileDest: "build/typescript/compile",
    rollupDest: "build/typescript/rollup",
    rollupTestDest: "build/typescript/rollupTest",
    babelDest: "build/typescript/babel",
    buildDest: "build/typescript/dest",
    // JVMのリソースコピー先
    resourceDest: "src/main/resources/public/"
}

gulp.task("clean", () => {
    return new Promise(() => {
        rimraf.sync(`${config.buildBase}`)
        rimraf.sync(`${config.resourceDest}/index.js`)
        rimraf.sync(`${config.resourceDest}/sw.js`)
    })
})

gulp.task("compile-typescript", () => {
    const project = gulpTypescript.createProject("tsconfig.json")
    return project.src()
                  .pipe(project())
                  .js
                  .pipe(gulp.dest(config.compileDest))
})

gulp.task("rollup-main", ["compile-typescript"], () => {
    return rollup.rollup({
        input: `${config.compileDest}/main.js`,
        plugins: [
            rollupNodeResolve(),
            rollupReplace({
                "process.env.NODE_ENV": JSON.stringify("production")
            }),
            rollupCommonJs({
                namedExports: {
                    "node_modules/lodash/lodash.js": ["concat", "isEmpty"],
                    "node_modules/react/index.js": ["createElement", "Component"],
                    "node_modules/react-dom/index.js": ["render"]
                }
            })
        ]
    }).then(bundle =>
        bundle.write({
            file: `${config.rollupDest}/index.js`,
            format: "iife",
            name: "main",
            sourcemap: true
        })
    )
})

gulp.task("rollup-sw", ["compile-typescript"], () => {
    return rollup.rollup({
        input: `${config.compileDest}/sw.js`,
        plugins: [
            rollupNodeResolve(),
            rollupReplace({
                "process.env.NODE_ENV": JSON.stringify("production")
            }),
            rollupCommonJs({
                namedExports: {
                    "node_modules/lodash/lodash.js": ["concat", "isEmpty"],
                    "node_modules/react/index.js": ["createElement", "Component"],
                    "node_modules/react-dom/index.js": ["render"]
                }
            })
        ]
    }).then(bundle =>
        bundle.write({
            file: `${config.rollupDest}/sw.js`,
            format: "iife",
            name: "main",
            sourcemap: true
        })
    )
})

gulp.task("babel", ["rollup-main", "rollup-sw"], () => {
    return gulp.src([
        `${config.rollupDest}/index.js`,
        `${config.rollupDest}/sw.js`
    ])
               .pipe(gulpBabel({
                   babelrc: false,
                   presets: ["env"],
                   compact: false
               }))
               .pipe(gulp.dest(config.babelDest))
})

gulp.task("copy", ["babel"], () => {
    return gulp.src([
        `${config.babelDest}/index.js`,
        `${config.babelDest}/sw.js`,
    ])
               .pipe(gulp.dest(config.buildDest))
})

// ビルドをJVMリソースとしてコピー
gulp.task("copy-to-resource", ["copy"], () => {
    return gulp.src(`${config.buildDest}/*`)
               .pipe(gulp.dest(config.resourceDest))
})

// ビルド
gulp.task("build", ["compile-typescript", "rollup-main", "rollup-sw", "copy", "copy-to-resource"])
gulp.task("default", ["build"])
