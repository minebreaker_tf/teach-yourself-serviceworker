package rip.deadcode

import com.google.gson.Gson
import org.flywaydb.core.Flyway
import org.h2.jdbcx.JdbcDataSource
import rip.deadcode.asashimo.Connectors
import spark.Request
import spark.Response
import spark.Spark.*

val dataSource = JdbcDataSource().apply {
    setURL("jdbc:h2:./.database")
    user = "sa"
    password = "sa"
}

val connector = Connectors.newInstance(dataSource)

val gson = Gson()

fun main(args: Array<String>) {

    migrateDb()
    igniteSpark()
}

fun migrateDb() {
    val flyway = Flyway().apply {
        this.dataSource = rip.deadcode.dataSource
    }
    flyway.migrate()
}

fun igniteSpark() {

    port(8080)

    staticFiles.location("public")
    staticFiles.expireTime(1)
    redirect.get("/", "/index.html")

    get("/api/todo", ::handleGetTodoList)
    post("/api/todo", ::handlePostTodo)
    delete("/api/todo/:id", ::handleDeleteTodo)
}

fun handleGetTodoList(req: Request, res: Response): String {
    val todo = getTodoList()
    return gson.toJson(todo)
}

fun handlePostTodo(req: Request, res: Response): String {

    @Suppress("UNCHECKED_CAST")
    val params = gson.fromJson(req.body(), Map::class.java) as Map<String, String>
    val id = addTodo(params["title"]!!)
    return gson.toJson(mapOf("id" to id, "title" to params["title"]))
}

fun handleDeleteTodo(req: Request, res: Response) {
    val id = req.params("id").toInt()
    deleteTodo(id)
}

data class Todo(
        val id: Int,
        val title: String
)

fun getTodoList() =
        connector.fetchAll("select id, title from todo", Todo::class)

fun addTodo(title: String): Int {
    connector.with {
        it["title"] = title
    }.exec("insert into todo values(null, :title)")
    return connector.fetch("select scope_identity()", Long::class).toInt()
}

fun deleteTodo(id: Int) {
    connector.with {
        it["id"] = id
    }.exec("delete from todo where id = :id")
}
