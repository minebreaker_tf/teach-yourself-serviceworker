"use strict";

(function () {
    'use strict';

    self.addEventListener("install", function (e) {
        e.waitUntil(caches.open("v1").then(function (cache) {
            return cache.addAll(["/", "/index.html", "/index.js"]);
        }));
    });
    self.addEventListener("activate", function (e) {
        e.waitUntil(caches.keys().then(function (keys) {
            return Promise.all(keys.map(function (key) {
                console.log(key);
                return Promise.resolve(true);
            }));
        }));
    });
    self.addEventListener("fetch", function (e) {
        e.respondWith(caches.match(e.request).then(function (cachedRes) {
            return cachedRes || fetch(e.request).then(function (res) {
                if (e.request.method === "GET" && res && res.ok) {
                    return caches.open("v1").then(function (cache) {
                        cache.put(e.request, res.clone());
                        return res;
                    });
                } else {
                    return res;
                }
            });
        }));
    });
})();
//# sourceMappingURL=sw.js.map