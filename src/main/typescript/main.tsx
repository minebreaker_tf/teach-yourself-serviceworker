import * as React from "react"
import * as ReactDOM from "react-dom"
import * as _ from "lodash"
import OnlineState from "./online-state"

interface Todo {
    id: number
    title: string
}

class Application extends React.Component<{}, {
    todoList: Array<Todo>
}> {

    constructor(props: {}) {
        super(props)
        this.state = {
            todoList: []
        }
        this.onAddRequest = this.onAddRequest.bind(this)
        this.onDeleteRequest = this.onDeleteRequest.bind(this)
    }

    componentDidMount() {
        fetch("/api/todo")
            .then(response => response.json())
            .then(json => this.setState({ todoList: json }))
    }

    private onAddRequest(title: string) {
        fetch("/api/todo", { method: "POST", body: JSON.stringify({ title }) })
            .then(response => response.json())
            .then(json => this.setState(prev => ({ todoList: _.concat(prev.todoList, json) })))
    }

    private onDeleteRequest(id: number) {
        fetch(`/api/todo/${id}`, { method: "DELETE" })
            .then(() => this.setState(prev => ({ todoList: prev.todoList.filter(todo => todo.id !== id) })))
    }

    render() {
        return (
            <div>
                <OnlineState />
                <AddTodo onAddRequest={this.onAddRequest} />
                <TodoList todoList={this.state.todoList} onDeleteRequest={this.onDeleteRequest} />
            </div>
        )
    }
}

function TodoList(props: { todoList: Array<Todo>, onDeleteRequest: (id: number) => void }) {

    if (_.isEmpty(props.todoList)) {
        return <p>Nothing to do!</p>
    }

    return (
        <ul>
            {props.todoList.map(todo => (
                <li key={todo.id}>
                    {todo.title}
                    <input type="button" value="delete" onClick={() => props.onDeleteRequest(todo.id)} />
                </li>
            ))}
        </ul>
    )
}

class AddTodo extends React.Component<{
    onAddRequest: (title: string) => void
}, any> {

    constructor(props: any) {
        super(props)
        this.state = {
            value: ""
        }
    }

    render() {
        return (
            <div>
                <input type="text" value={this.state.value} onChange={e => this.setState({ value: e.target.value })} />
                <input type="button" value="Send" onClick={() => {
                    this.props.onAddRequest(this.state.value);
                    this.setState({ value: "" })
                }} />
            </div>
        )
    }
}

function registerServiceWorker() {
    navigator.serviceWorker.register("/sw.js", { scope: "/" }).then(() => {
        console.log("Service worker registered.")
    }).catch(e => {
        console.log("Service worker registration failed: " + e)
    })
}

//noinspection JSUnusedGlobalSymbols
export default function main() {

    registerServiceWorker()

    ReactDOM.render(
        <Application />,
        document.getElementById("app")
    )
}
