import * as React from "react"

export default class OnlineState extends React.Component<{}, { online: boolean }> {

    constructor(props: any) {
        super(props)
        this.state = {
            online: navigator.onLine
        }
        this.updateState = this.updateState.bind(this)
    }

    private updateState() {
        this.setState({ online: navigator.onLine })
    }

    componentDidMount() {
        window.addEventListener("online", this.updateState)
        window.addEventListener("offline", this.updateState)
    }

    componentWillUnmount() {
        window.removeEventListener("online", this.updateState)
        window.removeEventListener("offline", this.updateState)
    }

    render() {
        return (
            <div>
                {this.state.online ? "Online" : "Offline"}
            </div>
        )
    }
}