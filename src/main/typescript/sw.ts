self.addEventListener("install", (e: any) => {
    e.waitUntil(
        caches.open("v1").then(cache =>
            cache.addAll([
                "/",
                "/index.html",
                "/index.js"
            ])
        )
    )
})

self.addEventListener("activate", (e: any) => {
    e.waitUntil(
        caches.keys().then(keys => Promise.all(keys.map(key => {
            //caches.delete(key)
            console.log(key)
            return Promise.resolve(true)
        })))
    )
})

self.addEventListener("fetch", (e: any) => {
    e.respondWith(
        caches.match(e.request).then(cachedRes => {
                return cachedRes || fetch(e.request).then(res => {
                    if (e.request.method === "GET" && res && res.ok) {
                        return caches.open("v1").then(cache => {
                            //noinspection JSIgnoredPromiseFromCall
                            cache.put(e.request, res.clone())
                            return res
                        })
                    } else {
                        return res
                    }
                })
            }
        )
    )
})
